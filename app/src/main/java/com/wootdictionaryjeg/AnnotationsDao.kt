package com.wootdictionaryjeg

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy

@Dao
interface AnnotationsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(annotationEntry: AnnotationEntity)
}