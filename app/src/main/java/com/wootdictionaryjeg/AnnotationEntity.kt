package com.wootdictionaryjeg

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ATTRIBUTES")
data class AnnotationEntity
    (
    @ColumnInfo(name = "key")
    var key: Int?,
    @ColumnInfo(name = "tagstring")
    var tagstring: String?,
    @ColumnInfo(name = "primaryKey")
    @PrimaryKey
    var primaryKey: Int?
)