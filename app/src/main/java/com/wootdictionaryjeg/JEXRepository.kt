package com.wootdictionaryjeg

import android.app.Application
import android.content.Context
import androidx.sqlite.db.SimpleSQLiteQuery

class JEXRepository(application: Application) {

    private var jexDao: JEXDao
    private var annotationsDao: AnnotationsDao
    private var database: JEXDatabase

    init {
        database = JEXDatabase.getDataBase(application.applicationContext)!!
        jexDao = database.jexDao()
        annotationsDao = database.annotationsDao()
    }

    fun insert(entry: JEXEntity) {
        jexDao.insert(entry)
    }

    fun insert(annotation: AnnotationEntity) {
        annotationsDao.insert(annotation)
    }

    fun update(entry: JEXEntity) {
        jexDao.update(entry)
    }

    fun getEqualEntries(searchword: String): List<Int> {
        return jexDao.getEqualEntries(searchword)
    }

    fun getStartWithEntries(searchword: String): List<Int> {
        return jexDao.getStartWithEntries(searchword)
    }

    fun getIncludesEntries(searchword: String): List<Int> {
        return jexDao.getIncludesEntries(searchword)
    }

    fun getEndWithEntries(searchword: String): List<Int> {
        return jexDao.getEndWithEntries(searchword)
    }


    fun getResultWords(key: Int) : List<JEXEntity> {
        return jexDao.getResultWords(key)
    }

    fun getAnnotations(key: Int) : List<AnnotationEntity> {
        return jexDao.getAnnotations(key)
    }

    fun clearAllTables() {
        database.clearAllTables()
    }

    fun resetDatabase(context: Context) {
        JEXDatabase.resetDatabase(context)
    }

    fun vacuum() {
        jexDao.vacuum(SimpleSQLiteQuery("VACUUM"))
    }
}




