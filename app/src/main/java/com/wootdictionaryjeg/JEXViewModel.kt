package com.wootdictionaryjeg

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*

class JEXViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: JEXRepository = JEXRepository(application)
    var annotMap = mapOf(
        "unclassified" to "" + "",
        "adjective (keiyoushi)" to "adj-i",
        "adjectival nouns or quasi-adjectives (keiyodoshi)" to "adj-na",
        "nouns which may take the genitive case particle `no" to "adj-no",
        "pre-noun adjectival (rentaishi)" to "adj-pn",
        "`taru' adjective" to "adj-t",
        "noun or verb acting prenominally (other than the above)" to "adj-f",
        "former adjective classification (being removed)" to "adj",
        "adverb (fukushi)" to "adv",
        "adverbial noun" to "adv-n",
        "adverb taking the `to' particle" to "adv-to",
        "auxiliary" to "aux",
        "auxiliary verb" to "aux-v",
        "auxiliary adjective" to "aux-adj",
        "conjunction" to "conj",
        "counter" to "ctr",
        "Expressions (phrases, clauses, etc.)" to "exp",
        "interjection (kandoushi)" to "int",
        "irregular verb" to "iv",
        "noun (common) (futsuumeishi)" to "n",
        "adverbial noun (fukushitekimeishi)" to "n-adv",
        "noun, used as a prefix" to "n-pref",
        "noun, used as a suffix" to "n-suf",
        "noun (temporal) (jisoumeishi)" to "n-t",
        "numeric" to "num",
        "pronoun" to "pn",
        "prefix" to "pref",
        "particle" to "prt",
        "suffix" to "suf",
        "Ichidan verb" to "v1",
        "Nidan verb with 'u ending (archaic)" to "v2a-s",
        "Yodan verb with `hu/fu ending (archaic)" to "v4h",
        "Yodan verb with `ru ending (archaic)" to "v4r",
        "Godan verb (not completely classified)" to "v5",
        "Godan verb - -aru special class" to "v5aru",
        "Godan verb with `bu ending" to "v5b",
        "Godan verb with `gu ending" to "v5g",
        "Godan verb with `ku ending" to "v5k",
        "Godan verb - iku/yuku special class" to "v5k-s",
        "Godan verb with `mu ending" to "v5m",
        "Godan verb with `nu ending" to "v5n",
        "Godan verb with `ru ending" to "v5r",
        "Godan verb with `ru ending (irregular verb)" to "v5r-i",
        "Godan verb with `su ending" to "v5s",
        "Godan verb with `tsu ending" to "v5t",
        "Godan verb with `u ending" to "v5u",
        "Godan verb with `u ending (special class)" to "v5u-s",
        "Godan verb - uru old class verb (old form of Eru)" to "v5uru",
        "Godan verb with `zu ending" to "v5z",
        "Ichidan verb - zuru verb - (alternative form of -jiru verbs)" to "vz",
        "intransitive verb" to "vi",
        "kuru verb - special class" to "vk",
        "irregular nu verb" to "vn",
        "noun or participle which takes the aux. verb suru" to "vs",
        "su verb - precursor to the modern suru" to "vs-c",
        "suru verb - irregular" to "vs-i",
        "suru verb - special class" to "vs-s",
        "transitive verb" to "vt",
        "rude or X-rated term" to "X",
        "abbreviation" to "abbr",
        "archaism" to "arch",
        "ateji (phonetic) reading" to "ateji",
        "children's language" to "chn",
        "colloquialism" to "col",
        "derogatory term" to "derog",
        "exclusively kanji" to "eK",
        "exclusively kana" to "ek",
        "familiar language" to "fam",
        "female term or language" to "fem",
        "gikun (meaning) reading" to "gikun",
        "honorific or respectful (sonkeigo) language" to "hon",
        "humble (kenjougo) language" to "hum",
        "word containing irregular kana usage" to "ik",
        "word containing irregular kanji usage" to "iK",
        "idiomatic expression" to "id",
        "irregular okurigana usage" to "io",
        "manga slang" to "m-sl",
        "male term or language" to "male",
        "male slang" to "male-sl",
        "word containing out-dated kanji" to "oK",
        "obsolete term" to "obs",
        "obscure term" to "obsc",
        "out-dated or obsolete kana usage" to "ok",
        "onomatopoeic or mimetic word" to "on-mim",
        "poetical term" to "poet",
        "polite (teineigo) language" to "pol",
        "rare (now replaced by \"obsc\")" to "rare",
        "sensitive word" to "sens",
        "slang" to "sl",
        "word usually written using kanji alone" to "uK",
        "word usually written using kana alone" to "uk",
        "vulgar expression or word" to "vulg"
    )

    var annotations = mutableListOf<List<AnnotationEntity>>()

    val result: MutableLiveData<MutableList<List<JEXEntity>>> by lazy {
        MutableLiveData<MutableList<List<JEXEntity>>>()
    }

    enum class SearchMethod {
        Match,
        StartWith,
        EndWith,
        Includes
    }

    var jap = true
    var en = true
    var ger = true
    var rus = true

    var searchMethod: SearchMethod = SearchMethod.Match

    var queryRunning = false

    fun dictionarySearch(searchword: String) = CoroutineScope(Job()).launch(Dispatchers.Default) {
        if (!queryRunning) {
            queryRunning = true
            result.postValue(null)
            val keyList = getKeyList(searchword)
            annotations = getAnnotationsList(keyList)
            result.postValue(getResults(keyList))
            queryRunning = false
        }
    }

    fun getKeyList(searchword: String): List<Int> {
        return when (searchMethod) {
            SearchMethod.Match -> getEqualEntries(searchword)
            SearchMethod.StartWith -> getStartWithEntries(searchword)
            SearchMethod.EndWith -> getEndWithEntries(searchword)
            SearchMethod.Includes -> getIncludesEntries(searchword)
        }
    }

    fun getResults(keyList: List<Int>): MutableList<List<JEXEntity>> {
        val resultList = mutableListOf<List<JEXEntity>>()
        for (key in keyList) {
            val wordsList = getResultWords(key)
            for (jex in wordsList) {
                if (annotMap.containsKey(jex.resultword)) {
                    jex.resultword = ""
                }
            }
            resultList.add(wordsList)
        }
        return resultList
    }

    fun insert(jexEntity: JEXEntity) {
        repository.insert(jexEntity)
    }

    fun insert(annotationEntity: AnnotationEntity) {
        repository.insert(annotationEntity)
    }

    fun update(jexEntity: JEXEntity) {
        repository.update(jexEntity)
    }

    fun clearAllTables() {
        repository.clearAllTables()
    }

    fun resetDatabase(context: Context) {
        repository.resetDatabase(context)
    }

    fun vacuum() {
        repository.vacuum()
    }

    fun getEqualEntries(searchword: String): List<Int> {
        return repository.getEqualEntries(searchword)
    }

    fun getStartWithEntries(searchword: String): List<Int> {
        return repository.getStartWithEntries(searchword)
    }

    fun getIncludesEntries(searchword: String): List<Int> {
        return repository.getIncludesEntries(searchword)
    }

    fun getEndWithEntries(searchword: String): List<Int> {
        return repository.getEndWithEntries(searchword)
    }

    fun getResultWords(key: Int): List<JEXEntity> {
        return repository.getResultWords(key)
    }

    fun getAnnotationsList(keyList: List<Int>): MutableList<List<AnnotationEntity>> {
        val annotationsList = mutableListOf<List<AnnotationEntity>>()
        for (key in keyList) {
            annotationsList.add(convertAnnotations(getAnnotations(key)))
        }
        return annotationsList
    }

    fun getAnnotations(key: Int): List<AnnotationEntity> {
        return repository.getAnnotations(key)
    }

    fun convertAnnotations(annotations: List<AnnotationEntity>): List<AnnotationEntity> {
        for (annotation in annotations) {
            annotation.tagstring = annotMap.getOrElse(
                annotation.tagstring.toString(), {
                    if (annotMap.containsValue(annotation.tagstring)) {
                        annotation.tagstring.toString()
                    } else ""
                })
        }
        return annotations
    }
}