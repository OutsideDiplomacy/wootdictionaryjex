package com.wootdictionaryjeg

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "JEX",
    indices = arrayOf(
        Index(
            value = ["key", "searchword", "resultword", "lan", "pri"],
            name = "COMPLETE"
        )
    )
)

data class JEXEntity
    (
    @ColumnInfo(name = "key")
    var key: Int?,
    @ColumnInfo(name = "searchword")
    var searchword: String?,
    @ColumnInfo(name = "pri")
    var pri: Int?,
    @ColumnInfo(name = "lan")
    var lan: String?,
    @ColumnInfo(name = "resultword")
    var resultword: String?,
    @ColumnInfo(name = "primaryKey")
    @PrimaryKey
    var primaryKey: Int
)