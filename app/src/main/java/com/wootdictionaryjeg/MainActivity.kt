package com.wootdictionaryjeg

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var model: JEXViewModel
    private lateinit var adapter: JEXAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Utility().setToolbar(this)
        Utility().addHelpMenu(this)
        model = ViewModelProviders.of(this).get(JEXViewModel::class.java)
        adapter = JEXAdapter(model, this)
        recyclerView = findViewById(R.id.result_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
        model.result.observe(this, Observer { result ->
            recyclerView.adapter = JEXAdapter(model, this)
            recyclerView.visibility = View.VISIBLE
        })

        //テキスト入力
        val input = findViewById<EditText>(R.id.editText)
        val clear = findViewById<TextView>(R.id.clear)
        if (input.text.isEmpty()) input.hint = getString(R.string.hint)
        input.addTextChangedListener(object : TextWatcher {
            private val timer = Timer()
            override fun afterTextChanged(s: Editable) {
                //enter pressed
                if (input.text.endsWith("\r") || input.text.endsWith("\n")) {
                    model.dictionarySearch((input.text.toString()))
                } else if (input.text.isEmpty()) {
                    input.hint = getString(R.string.hint)
                    clear.setText("  X  ")
                } else {
                    input.hint = ""
                    clear.setText("  X  ")
                    timer.schedule(object : TimerTask() {
                        override fun run() {
                            model.dictionarySearch((input.text.toString()))
                        }
                    }, 1500)
                }

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        clear.setOnClickListener {
            input.text.clear()
        }

        //Enter Key
        input.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                model.dictionarySearch("" + input.text)
                return@OnKeyListener true
            }
            false
        })
        val searchButton = findViewById<Button>(R.id.SearchButton)
        searchButton.setOnClickListener {
            model.dictionarySearch((input.text.toString()))
        }

        //Search Method
        val methodChanger = findViewById<RadioGroup>(R.id.method_changer)
        methodChanger.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.Matches -> model.searchMethod = JEXViewModel.SearchMethod.Match
                R.id.Includes -> model.searchMethod = JEXViewModel.SearchMethod.Includes
                R.id.StartsWith -> model.searchMethod = JEXViewModel.SearchMethod.StartWith
                R.id.EndsWith -> model.searchMethod = JEXViewModel.SearchMethod.EndWith
            }
            if (!input.text.isEmpty()) {
                model.dictionarySearch((input.text.toString()))
            }
        }
        findViewById<RadioButton>(R.id.Matches).setOnClickListener({})
        findViewById<RadioButton>(R.id.StartsWith).setOnClickListener({})
        findViewById<RadioButton>(R.id.Includes).setOnClickListener({})
        findViewById<RadioButton>(R.id.EndsWith).setOnClickListener({})

        //Languages
        val jpCheckbox = findViewById<CheckBox>(R.id.JP)
        jpCheckbox.setOnClickListener {
            model.jap = jpCheckbox.isChecked
            recyclerView.adapter = adapter
        }
        val enCheckbox = findViewById<CheckBox>(R.id.EN)
        enCheckbox.setOnClickListener {
            model.en = enCheckbox.isChecked
            recyclerView.adapter = adapter
        }
        val deCheckbox = findViewById<CheckBox>(R.id.DE)
        deCheckbox.setOnClickListener {
            model.ger = deCheckbox.isChecked
            recyclerView.adapter = adapter
        }
        val ruCheckbox = findViewById<CheckBox>(R.id.RUS)
        ruCheckbox.setOnClickListener {
            model.rus = ruCheckbox.isChecked
            recyclerView.adapter = adapter
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}
