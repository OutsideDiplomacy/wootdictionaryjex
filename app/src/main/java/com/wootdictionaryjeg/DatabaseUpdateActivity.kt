package com.wootdictionaryjeg

import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.coroutines.*
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPFile
import org.xml.sax.Attributes
import org.xml.sax.InputSource
import org.xml.sax.Locator
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import java.io.*
import java.lang.Integer.parseInt
import java.text.DecimalFormat
import java.util.zip.GZIPInputStream
import javax.xml.parsers.SAXParserFactory

class DatabaseUpdateActivity : AppCompatActivity() {

    lateinit var model: JEXViewModel
    lateinit var job: Job
    lateinit var location: Locator
    var abort = false
    var complete = false
    lateinit var builder: android.app.AlertDialog.Builder


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            abortUpdate()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    class SAXTerminatorException : SAXException() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_database_update)
        Utility().setToolbar(this)
        Utility().addHelpMenu(this)
        builder = android.app.AlertDialog.Builder(this)
        if (Utility().isInternetAvailable(applicationContext)) {
            model = ViewModelProviders.of(this).get(JEXViewModel::class.java)
            val owl = findViewById<TextView>(R.id.owl)
            owl.setText(R.string.owl)
            builder.setMessage(R.string.startUpdateDialog)
            builder.setCancelable(false)
            builder.setPositiveButton(
                R.string.updateLabel
            ) { dialog, id ->
                dialog.cancel()
                complete = false
                job = CoroutineScope(Job()).launch(Dispatchers.Default) {
                    buildDatabase(unzipFile(downloadZip()), model)
                }
            }
            builder.setNegativeButton(
                R.string.cancelLabel
            ) { dialog, id ->
                dialog.cancel()
                finish()
            }
            val alert: android.app.AlertDialog = builder.create()
            alert.show()
        } else {
            builder.setMessage("No internet connection")
            builder.setCancelable(false)
            builder.setPositiveButton(
                R.string.OK
            ) { dialog, id ->
                dialog.cancel()
                finish()
            }
            val alert: android.app.AlertDialog = builder.create()
            alert.show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::job.isInitialized) {
            job.cancel()
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun abortUpdate() {
        if (!complete) {
            builder.setMessage(R.string.abortUpdateDialog)
            builder.setCancelable(true)
            builder.setPositiveButton(
                R.string.abortLabel
            ) { dialog, id ->
                dialog.cancel()
                job.cancel()
                abort = true
                model.resetDatabase(applicationContext)
                finish()
            }
            builder.setNegativeButton(
                R.string.continueLabel
            ) { dialog, id ->
                dialog.cancel()
            }
            val alert: android.app.AlertDialog = builder.create()
            alert.show()
        }
    }

    fun downloadZip(): File {
        runOnUiThread() {
            findViewById<TextView>(R.id.updateStatusText).setText(R.string.downloading)
            findViewById<TextView>(R.id.updateStatusPercent).setText("0%")
            findViewById<ProgressBar>(R.id.progressBarHorizontal).visibility = View.VISIBLE
        }
        val storageType = "JMdictStorage"
        val fileNameJMDictGz = "JMdict.gz"
        val savePathGz = File(getExternalFilesDir(storageType), fileNameJMDictGz)
        val ftpMonashEduAu = "130.194.20.68"
        val pathNameJMDictGz = "pub/nihongo/"

        try {
            val ftpClient = FTPClient()
            ftpClient.connect(ftpMonashEduAu)
            ftpClient.login("anonymous", "nobody")
            ftpClient.enterLocalPassiveMode()
            ftpClient.changeWorkingDirectory(pathNameJMDictGz)
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
            val outputStream = FileOutputStream(savePathGz)
            var fileSize: Long = 0
            var fileSizeMB: Double = 0.0
            val file: Array<FTPFile> = ftpClient.listFiles(fileNameJMDictGz)
            if (file.size == 1 && file[0].isFile) {
                fileSize = file[0].size
                fileSizeMB = fileSize.toDouble() / (1024 * 1024).toDouble()
            }
            val inputStream = ftpClient.retrieveFileStream(fileNameJMDictGz)
            val buffer = ByteArray(256)
            var bytesRead = -1
            var downloadedSize = 0
            val fileSizeMbFormat = DecimalFormat("##.#").format(fileSizeMB)
            while (inputStream.read(buffer).also({ bytesRead = it }) != -1) {
                outputStream.write(buffer, 0, bytesRead)
                runOnUiThread() {
                    downloadedSize += 256
                    val prog: Double = (downloadedSize.toDouble() / fileSize.toDouble()) * 100
                    findViewById<ProgressBar>(R.id.progressBarHorizontal).setProgress(prog.toInt())
                    val downloadedSizeMB = DecimalFormat("##.#")
                        .format(downloadedSize.toDouble() / (1024 * 1024))
                    findViewById<TextView>(R.id.updateStatusPercent).text =
                        if (prog < 100) prog.toInt().toString() + "% (" + downloadedSizeMB + "/" + fileSizeMbFormat + "MB)"
                        else "100% (" + fileSizeMbFormat + "/" + fileSizeMbFormat + "MB)"
                }
            }
            outputStream.close()
            inputStream.close()
            runOnUiThread {
                Toast.makeText(applicationContext, R.string.downloadCompleteMsg, Toast.LENGTH_LONG)
                    .show()
            }

        } catch (ex: IOException) {
            runOnUiThread() {
                Toast.makeText(applicationContext, R.string.downloadFailed, Toast.LENGTH_LONG)
                    .show()
                abort = true
                job.cancel()
                finish()
            }
            ex.printStackTrace()
        }
        return savePathGz
    }

    fun unzipFile(savePathGz: File): File {
        val savePathXML = File(getExternalFilesDir("JMdictStorage"), "JMdict.xml")
        try {
            runOnUiThread() {
                findViewById<TextView>(R.id.updateStatusText).setText(R.string.unzipping)
            }
            val gzis = GZIPInputStream(FileInputStream(savePathGz))
            val fileSize = savePathGz.length()
            var unzippedSize = 0
            val out = FileOutputStream(savePathXML)
            val buffer = ByteArray(1024)
            var totalSize = -1
            while (gzis.read(buffer).also({ totalSize = it }) > 0) {
                out.write(buffer, 0, totalSize)
                runOnUiThread() {
                    unzippedSize += totalSize / 5
                    val prog = (unzippedSize.toDouble() / fileSize.toDouble()) * 100
                    findViewById<ProgressBar>(R.id.progressBarHorizontal).setProgress(prog.toInt())
                    findViewById<TextView>(R.id.updateStatusPercent).text =
                        if (prog < 100) prog.toInt().toString() + "%" else "100%"
                }
            }
            gzis.close()
            out.close()
            if (!abort) {
                runOnUiThread() {
                    Toast.makeText(
                        applicationContext,
                        R.string.unzippingCompleteMsg,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
            if (!abort && !isFinishing) {
                runOnUiThread() {
                    Toast.makeText(applicationContext, R.string.unzippingFailed, Toast.LENGTH_LONG)
                        .show()
                    job.cancel()
                    abort = true
                    finish()
                }
            }
        }
        savePathGz.delete()
        return savePathXML
    }

    enum class Tag() {
        ent_seq, reb, re_pri, keb, ke_pri, gloss, pos
    }

    fun removeBrackets(word: String): String {
        return word.replace("\\p{P}", " ", true)
    }

    val priorityList = listOf("news1", "ichi1", "spec1", "spec2", "gai1")
    fun getPriority(pri: String?): Int {
        if (priorityList.contains(pri)) {
            return 1
        } else return 0
    }

    fun buildDatabase(savePathXML: File, model: JEXViewModel) {
        if (!abort && !isFinishing) {
            runOnUiThread() {
                val anim: Animation = AlphaAnimation(0.0f, 1.0f)
                anim.duration = 1000
                anim.startOffset = 20
                anim.repeatMode = Animation.REVERSE
                anim.repeatCount = Animation.INFINITE
                findViewById<TextView>(R.id.owl).startAnimation(anim)
                findViewById<TextView>(R.id.updateStatusText).setText(R.string.building)
                findViewById<TextView>(R.id.updateStatusPercent).setText("0%")
                findViewById<ProgressBar>(R.id.progressBarHorizontal).progress = 0
            }
            model.clearAllTables()
            try {
                val saxParser = SAXParserFactory.newInstance().newSAXParser()
                val lineNumberReader = LineNumberReader(FileReader(savePathXML))
                lineNumberReader.skip((Integer.MAX_VALUE.toLong()))
                val totalLines = (lineNumberReader.getLineNumber() + 1).toDouble()
                val inputStream: InputStream = FileInputStream(savePathXML)
                val reader: Reader = InputStreamReader(inputStream, "UTF-8")
                val source = InputSource(reader)
                source.encoding = "UTF-8"
                var tag: Tag = Tag.ent_seq
                var ent_seq = 0
                var lang: String? = ""
                var reading = ""
                var re_pri: String? = ""
                var kanji = ""
                var ke_pri: String? = ""
                var pkey = 0

                val handler: DefaultHandler = object : DefaultHandler() {

                    override fun setDocumentLocator(locator: Locator?) {
                        super.setDocumentLocator(locator)
                        if (locator != null) {
                            location = locator
                        }
                    }

                    @Throws(SAXException::class)
                    override fun startElement(
                        uri: String?,
                        localName: String?,
                        qName: String,
                        attributes: Attributes?
                    ) {
                        when (qName) {
                            "ent_seq" -> tag = Tag.ent_seq
                            "reb" -> tag = Tag.reb
                            "re_pri" -> tag = Tag.re_pri
                            "keb" -> tag = Tag.keb
                            "ke_pri" -> tag = Tag.ke_pri
                            "gloss" -> {
                                tag = Tag.gloss
                                lang = attributes?.getValue("lang").toString()
                            }
                            "pos" -> tag = Tag.pos
                        }
                        if (abort) {
                            throw SAXTerminatorException()
                        }
                    }

                    @Throws(SAXException::class)
                    override fun characters(
                        ch: CharArray?,
                        start: Int,
                        length: Int
                    ) {
                        val chars = (String(ch!!, start, length))
                        if (chars.trim().isNotEmpty()) {
                            when (tag) {
                                Tag.ent_seq -> {
                                    if (chars.trim().isNotEmpty()) {
                                        ent_seq = parseInt(chars)
                                    }
                                }
                                Tag.reb -> {
                                    reading = chars
                                    model.insert(
                                        JEXEntity(
                                            ent_seq,
                                            reading,
                                            getPriority(re_pri),
                                            "R",
                                            removeBrackets(reading),
                                            pkey
                                        )
                                    )
                                    pkey += 1
                                }
                                Tag.re_pri -> {
                                    model.update(
                                        JEXEntity(
                                            ent_seq,
                                            reading,
                                            getPriority(chars),
                                            "R",
                                            removeBrackets(reading),
                                            pkey
                                        )
                                    )
                                }
                                Tag.keb -> {
                                    kanji = chars
                                    model.insert(
                                        JEXEntity(
                                            ent_seq,
                                            kanji,
                                            getPriority(ke_pri),
                                            "K",
                                            removeBrackets(kanji),
                                            pkey
                                        )
                                    )
                                    pkey += 1
                                }
                                Tag.ke_pri -> {
                                    model.update(
                                        JEXEntity(
                                            ent_seq,
                                            kanji,
                                            getPriority(chars),
                                            "K",
                                            removeBrackets(kanji),
                                            pkey
                                        )
                                    )
                                }
                                Tag.gloss -> {
                                    if (lang == "eng" || lang == "ger" || lang == "rus") {
                                        if (!model.annotMap.containsKey(chars)) {
                                            model.insert(
                                                JEXEntity(
                                                    ent_seq,
                                                    chars,
                                                    0,
                                                    lang,
                                                    removeBrackets(chars),
                                                    pkey
                                                )
                                            )
                                            pkey += 1
                                        }
                                    }
                                }
                                Tag.pos -> {
                                    if (model.annotMap.containsKey(chars)) {
                                        model.insert(
                                            AnnotationEntity(
                                                ent_seq,
                                                model.annotMap.getValue(chars),
                                                pkey
                                            )
                                        )
                                        pkey += 1
                                    }
                                }
                            }

                            if (location.lineNumber % 25000 == 0) {
                                val pos = location.lineNumber.toDouble()
                                val prog = (pos / totalLines) * 100
                                runOnUiThread() {
                                    findViewById<ProgressBar>(R.id.progressBarHorizontal).progress =
                                        prog.toInt()
                                    findViewById<TextView>(R.id.updateStatusPercent).text =
                                        if (prog < 100) prog.toInt().toString() + "%" else "100%"
                                }
                            }
                        }

                    }
                }

                if (!abort) {
                    saxParser.parse(source, handler)
                }
            } catch (e: SAXTerminatorException) {
                e.printStackTrace()
                if (!abort) {
                    runOnUiThread() {
                        Toast.makeText(
                            applicationContext,
                            R.string.databaseCompilingFailed,
                            Toast.LENGTH_LONG
                        )
                            .show()
                        job.cancel()
                        abort = true
                        model.resetDatabase(applicationContext)
                        finish()
                    }
                }
            }
            savePathXML.delete()
            if (!abort) {
                runOnUiThread() {
                    Toast.makeText(applicationContext, R.string.readyMsg, Toast.LENGTH_LONG).show()
                    findViewById<TextView>(R.id.updateStatusText).setText(R.string.ready)
                    findViewById<TextView>(R.id.owl).clearAnimation()
                    findViewById<ProgressBar>(R.id.progressBarHorizontal).progress = 100
                    findViewById<TextView>(R.id.updateStatusPercent).text = "100%"
                    builder.setMessage(R.string.updateCompleted)
                    builder.setCancelable(false)
                    builder.setPositiveButton(
                        R.string.OK
                    ) { dialog, id ->
                        dialog.cancel()
                        finish()
                    }
                    val alert: android.app.AlertDialog = builder.create()
                    alert.show()
                }
            }
            model.vacuum()
        }
    }
}