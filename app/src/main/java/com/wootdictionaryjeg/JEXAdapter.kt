package com.wootdictionaryjeg

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.listviewitem.view.*

class JEXAdapter(var model: JEXViewModel, val context: Context) :
    RecyclerView.Adapter<JEXAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        if (model.result.value != null) {
            return model.result.value!!.size
        } else {
            return 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.listviewitem, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (model.result.value != null) {
            var kanji = ""
            var reading = ""
            var annot = ""
            var english = ""
            var german = ""
            var russian =""
            var sep = "; "
            var key: Int? = 0
            for (entity in model.result.value!!.get(position)) {
                key = entity.key
                when (entity.lan) {
                    "K" -> kanji = addResult(kanji, entity.resultword, sep)
                    "R" -> reading = addResult(reading, entity.resultword, sep)
                    "eng" -> english = addResult(english, entity.resultword, sep)
                    "ger" -> german = addResult(german, entity.resultword, sep)
                    "rus" -> russian = addResult(russian, entity.resultword, sep) + "\r\n"
                }
            }

            holder.itemView.count_label.setText("(" + (position + 1) + ")")

            if (model.jap) {
                //When there is no kanji to show, use kanji view for reading without brackets and
                //hide reading view
                if (!kanji.isEmpty()) {
                    holder.itemView.kanji.setText(kanji.dropLast(sep.length))
                    holder.itemView.reading.setText("【" + reading.dropLast(sep.length) + "】")
                } else {
                    holder.itemView.kanji.setText(reading.dropLast(sep.length))
                }
                sep = ", "
                for (annotList in model.annotations) {
                    for (annotation in annotList) {
                        if (annotation.key != key) {
                            break
                        } else {
                            annot = addResult(annot, annotation.tagstring, sep)
                        }
                    }
                }
                if (annot.isNotEmpty()) {
                    holder.itemView.annotation_reading.setText("{" + annot.dropLast(sep.length) + "}.")
                    holder.itemView.annotation_kanji.setText("{" + annot.dropLast(sep.length) + "}.")
                }
                holder.itemView.reading_row.visibility =
                    if (kanji.isEmpty()) View.GONE else View.VISIBLE

                if (holder.itemView.reading_row.isVisible) {
                    holder.itemView.annotation_kanji.visibility =
                        if (!kanji.isEmpty() && kanji.length + annot.length > 25) View.GONE else View.VISIBLE
                } else {
                    holder.itemView.annotation_kanji.visibility = View.VISIBLE
                }
                holder.itemView.annotation_reading.visibility =
                        //will push annot out of screen if kanji replaced by reading that is too long
                    if (holder.itemView.annotation_kanji.isVisible) View.GONE else View.VISIBLE
            } else {
                holder.itemView.japanese_rows.visibility = View.GONE
            }

            if (model.en && !english.isEmpty()) {
                holder.itemView.english.setText(english.dropLast(sep.length) + ".")
                holder.itemView.english_row.visibility = View.VISIBLE
            } else {
                holder.itemView.english_row.visibility = View.GONE
            }

            if (model.ger && !german.isEmpty()) {
                holder.itemView.german.setText(german.dropLast(sep.length) + ".")
                holder.itemView.german_row.visibility = View.VISIBLE
            } else {
                holder.itemView.german_row.visibility = View.GONE
            }

            if (model.rus && !russian.isEmpty()) {
                holder.itemView.russian.setText(russian.dropLast(sep.length + 2) + ".")
                holder.itemView.russian_row.visibility = View.VISIBLE
            } else {
                holder.itemView.russian_row.visibility = View.GONE
            }
        }
    }

    private fun addResult(thisResult: String, nextResult: String?, sep: String): String {
        var newString = thisResult
        if (!thisResult.contains(nextResult.toString())) {
            newString += nextResult + sep
        }
        return newString
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }
}

