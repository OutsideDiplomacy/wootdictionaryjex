package com.wootdictionaryjeg

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class HelpAboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_about)
        Utility().setToolbar(this)
        Utility().addHelpMenu(this)
        setText(intent)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun setText(intent: Intent) {
        val help = findViewById<TextView>(R.id.helpAboutTextView)
        val text = getResources().openRawResource(R.raw.helpabout).bufferedReader().readLines()
        var instructions = ""
        var annotations = ""
        var licenses = ""
        var string = ""
        for (line in text) {
            if (line.startsWith("ANNOTATIONS")) {
                instructions = string
                string = ""
            }
            if (line.startsWith("LICENSES")) {
                annotations = string
                string = ""
            }
            string += line + "\r\n"
        }
        licenses = string
        when (intent.getIntExtra("MENU_ITEM", R.id.how_to_use)) {
            R.id.how_to_use -> help.text = instructions
            R.id.annotations -> help.text = annotations
            R.id.licenses -> help.text = licenses
        }
        help.movementMethod = ScrollingMovementMethod()
    }
}
