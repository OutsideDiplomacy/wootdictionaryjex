package com.wootdictionaryjeg

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class Utility() {
    fun addHelpMenu(context: AppCompatActivity) {
        context.setSupportActionBar(context.findViewById(R.id.toolbar))
        context.findViewById<TextView>(R.id.helpAbout).setOnClickListener {
            val popup = PopupMenu(context, context.findViewById(R.id.helpAbout))
            val inflater: MenuInflater = popup.menuInflater
            inflater.inflate(R.menu.help_about_menu, popup.menu)
            popup.setOnMenuItemClickListener { item: MenuItem? ->
                if (context is DatabaseUpdateActivity && !item?.itemId?.equals(R.id.update)!!) {
                    context.abortUpdate()
                } else if (item?.itemId?.equals(R.id.dictionary)!!) {
                    if (context !is MainActivity) {
                        ContextCompat.startActivity(
                            context,
                            Intent(context, MainActivity::class.java),
                            null
                        )
                    }
                } else if (item.itemId.equals(R.id.update)) {
                    if (context !is DatabaseUpdateActivity) {
                        ContextCompat.startActivity(
                            context,
                            Intent(context, DatabaseUpdateActivity::class.java),
                            null
                        )
                    }
                } else {
                    if (context !is HelpAboutActivity) {
                        ContextCompat.startActivity(
                            context,
                            Intent(context, HelpAboutActivity::class.java).putExtra(
                                "MENU_ITEM",
                                item.itemId
                            ), null
                        )
                    } else {
                        context.setText(
                            Intent(context, HelpAboutActivity::class.java).putExtra(
                                "MENU_ITEM",
                                item.itemId
                            )
                        )
                    }
                }
                true
            }
            popup.show()
        }
    }

    fun setToolbar(context: AppCompatActivity) {
        context.setSupportActionBar(context.findViewById(R.id.toolbar))
        context.getSupportActionBar()?.setIcon(R.drawable.ic_launcher);
    }

    //https://stackoverflow.com/questions/53532406/activenetworkinfo-type-is-deprecated-in-api-level-28
    fun isInternetAvailable(context: Context): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        return result
    }
}