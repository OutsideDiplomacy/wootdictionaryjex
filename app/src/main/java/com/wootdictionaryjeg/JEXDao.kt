package com.wootdictionaryjeg

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery

@Dao
interface JEXDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(jexEntry: JEXEntity)

    @Update
    fun update(jexEntry: JEXEntity)

    @Query("SELECT DISTINCT key FROM JEX WHERE searchword = :searchword order by pri desc LIMIT 300")
    fun getEqualEntries(searchword: String): List<Int>

    @Query("SELECT DISTINCT key FROM JEX WHERE searchword like :searchword || '%' LIMIT 50")
    fun getStartWithEntries(searchword: String): List<Int>

    @Query("SELECT DISTINCT key FROM JEX WHERE searchword like '%' || :searchword LIMIT 50")
    fun getEndWithEntries(searchword: String): List<Int>

    @Query("SELECT DISTINCT key FROM JEX WHERE searchword like '%' || :searchword || '%' LIMIT 50")
    fun getIncludesEntries(searchword: String): List<Int>

    @Query("SELECT * FROM JEX WHERE key = :key and (lan = 'K' or lan = 'R' or lan = 'eng' or lan= 'ger' or lan = 'rus') order by lan ='K' desc, lan = 'R' desc, lan='eng' desc, lan ='ger' desc, lan = 'rus' desc, pri desc")
    fun getResultWords(key: Int): List<JEXEntity>

    @Query("SELECT * FROM ATTRIBUTES WHERE key =:key")
    fun getAnnotations(key: Int): List<AnnotationEntity>

    @RawQuery
    fun vacuum(supportSQLiteQuery: SupportSQLiteQuery?): Int
}