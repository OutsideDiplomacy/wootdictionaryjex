# wootdictionaryJEG
Renewal of JMDict based dictionary app, will include:
1. Move from Java to Kotlin under MVVM Architecture
2. Priority of common Japanese entries
3. New database file (https://github.com/ProfessorRino/JMDict2SQLite)
4. Incremental database update function
5. Tests
